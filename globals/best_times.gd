extends Node
class_name BestTimesNode

@onready var resource := HighScoreResource.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	resource.load_config()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
