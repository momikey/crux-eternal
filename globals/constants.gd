extends Node
class_name Types

# The version info for the game as a whole.
@export var version_info : VersionInfoResource

# The types of cell
enum CellType {
	SUM = 0,
	ENTRY = 1
}

# The size of a cell in pixels
const CELL_SIZE := 64

# The various screen types.
enum Screens {
	none,
	main_menu,
	board_select,
	puzzle,
	options,
	credits
}

# The "cost" of a hint, in seconds
const HINT_COST := 15

# Mapping scenes to the screen types.

@export var main_menu_scene : PackedScene
@export var board_select_scene : PackedScene
@export var puzzle_scene : PackedScene
@export var options_scene : PackedScene
@export var credits_scene : PackedScene


@onready var screen_scenes := {
	Screens.main_menu: main_menu_scene,
	Screens.board_select: board_select_scene,
	Screens.puzzle: puzzle_scene,
	Screens.options: options_scene,
	Screens.credits: credits_scene
}
