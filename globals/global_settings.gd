extends Node

# The settings object for game-level settings and preferences.
@export var settings : GameSettings

# Reference to the master audio bus.
@onready var master_audio_bus := AudioServer.get_bus_index("Master")

const RESOURCE_NAME := "user://crux_settings.tres"


func load_settings():
	var has_settings := false
	if settings == null:
		settings = GameSettings.new()
	if ResourceLoader.exists(RESOURCE_NAME, "GameSettings"):
		var data = ResourceLoader.load(RESOURCE_NAME)
		if data is GameSettings:
			has_settings = true
			settings = GameSettings.new()
			settings.update_from(data)
	if not has_settings:
		save_settings()
	
	
func save_settings():
	var result := ResourceSaver.save(settings, RESOURCE_NAME)
	if (result != OK):
		# TODO: Some kind of error handling if we can't save the prefs file.
		breakpoint


func change_main_volume(new_value):
	# Handle changing the main volume by changing the master bus volume in code.
	settings.main_volume = new_value
	if settings.main_volume == 0:
		AudioServer.set_bus_mute(master_audio_bus, true)
	else:
		AudioServer.set_bus_mute(master_audio_bus, false)
		# Convert between human volume level and audiophile dB scale.
		AudioServer.set_bus_volume_db(master_audio_bus, -(3 * (10 - settings.main_volume)))


func _ready():
	load_settings()
	change_main_volume(settings.main_volume)
