extends Node
class_name SceneManager

var root = null
var current_screen : GameScreen
var is_changing := false


# Called when the node enters the scene tree for the first time.
func _ready():
	root = get_tree().get_root()
	if root.get_children().back() is GameScreen:
		current_screen = root.get_children().back()


func change_screen(screen_type: Types.Screens, res: Resource = null):
	var new_screen = Constants.screen_scenes[screen_type]
	call_deferred("_deferred_change_screen", new_screen, res)
	

func _deferred_change_screen(new_screen: PackedScene, res: Resource = null):
	# This follows the Godot manual's suggestion.
	
	if current_screen != null:
		current_screen.free()
	current_screen = new_screen.instantiate()
	
	get_tree().root.add_child(current_screen)
	current_screen._screen_setup(res)
	get_tree().current_scene = current_screen
