extends Node

static func format(time: int):
	var formatted_time : String
	var seconds = time % 60
	var minutes = time / 60
	var hours = minutes / 60
	if hours > 0:
		minutes = minutes % 60
	
	var seconds_string := ("0" if seconds < 10 else "") + str(seconds)
	var minutes_string := ("0" if minutes < 10 else "") + str(minutes)
	var hours_string := ("0" if hours < 10 else "") + str(hours)
	
	if hours == 0:
		formatted_time = ":".join([minutes_string, seconds_string])
	else:
		formatted_time = ":".join([hours_string, minutes_string, seconds_string])

	return formatted_time
