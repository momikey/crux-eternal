extends GridContainer
class_name PuzzleBoard

signal play_sound(sound_name: String)
signal cell_correct
signal cell_incorrect
signal game_incomplete
signal game_complete

var entry_cell_node := preload("res://nodes/entry_cell.tscn")
var sum_cell_node := preload("res://nodes/sum_cell.tscn")

@export var board_layout : Array[int] = [
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1,
	0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1,
	0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1,
	0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
	0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1,
	0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
	0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1,
	0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1,
	0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
	0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0,
	0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1,
	0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1,
	0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1,
	0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1
]

var child_cells : Array[BoardCell]
var selected_cell : EntryCell
var rng := RandomNumberGenerator.new()

# The probabilities for each digiit in ordeer. Note that 0 should always have 0 probabiltity.
@export var digit_probabilities = [0, 8, 6, 5, 4, 3, 4, 5, 6, 8]

# Cell access functinons.

func cell_at(x: int, y: int) -> BoardCell:
	if x >= columns or x < 0 or y >= columns or y < 0:
		return null
	else:
		return child_cells[y*columns + x]
		
		
func cell_above(cell: BoardCell) -> BoardCell:
	if cell.y_position == 0:
		return null
	else:
		return cell_at(cell.x_position, cell.y_position - 1)
		

func cell_below(cell: BoardCell) -> BoardCell:
	if cell.y_position == columns - 1:
		return null
	else:
		return cell_at(cell.x_position, cell.y_position + 1)


func cell_left(cell: BoardCell) -> BoardCell:
	if cell.x_position == 0:
		return null
	else:
		return cell_at(cell.x_position - 1, cell.y_position)
		

func cell_right(cell: BoardCell) -> BoardCell:
	if cell.x_position == columns - 1:
		return null
	else:
		return cell_at(cell.x_position + 1, cell.y_position)
		
		
func sum_right(cell: SumCell) -> int:
	var next := cell_right(cell)
	var total := 0
	while next is EntryCell:
		total += next.correct
		next = cell_right(next)
	return total


func sum_down(cell: SumCell) -> int:
	var next := cell_below(cell)
	var total := 0
	while next is EntryCell:
		total += next.correct
		next = cell_below(next)
	return total
	
	
func entries_left(cell: EntryCell) -> Array[EntryCell]:
	var row : Array[EntryCell] = []
	var next := cell_left(cell)
	while next is EntryCell:
		row.push_back(next)
		next = cell_left(next)
	return row
	

func entries_right(cell: EntryCell) -> Array[EntryCell]:
	var row : Array[EntryCell] = []
	var next := cell_right(cell)
	while next is EntryCell:
		row.push_back(next)
		next = cell_right(next)
	return row


func entries_above(cell: EntryCell) -> Array[EntryCell]:
	var col : Array[EntryCell] = []
	var next := cell_above(cell)
	while next is EntryCell:
		col.push_back(next)
		next = cell_above(next)
	return col
	
	
func entries_below(cell: EntryCell) -> Array[EntryCell]:
	var col : Array[EntryCell] = []
	var next := cell_below(cell)
	while next is EntryCell:
		col.push_back(next)
		next = cell_below(next)
	return col
	
	
# Choose a number from an array of weights.
func weighted_choice(arr):
	var total = arr.reduce(func(acc, num): return acc + num)
	var n = rng.randi_range(1, total)
	var running := 0
	for i in range(arr.size()):
		running += arr[i]
		if running >= n:
			return i
	
	# Shouldn't happen
	return -1
	
	
func generate_cell(cell: EntryCell):
	var weights := digit_probabilities.duplicate()
	var surrounding_cells = entries_left(cell) + entries_above(cell)
	for c in surrounding_cells:
		# Give a weight bonus to adjacent numbers.
		if c.correct > 1:
			weights[c.correct - 1] *= 2
		if c.correct < 9:
			weights[c.correct + 1] /= 2

		# Give a smaller bonus to "opposite" numbers: 1-9, 2-8, etc.
		weights[10 - c.correct] += 3
			
	for c in surrounding_cells:
		# Clear weights of surrounding cells, so they can't be generated.
		weights[c.correct] = 0
	
	# If all weights have been cleared, there's no possible number to use here,
	# which means generation has failed for some reason. In that case, back up
	# over the row and try again.
	if weights.all(func(n): return n == 0):
		print("Unable to generate board at (%d,%d), restarting row and column" % [cell.x_position, cell.y_position])
		var row := entries_left(cell) + entries_above(cell)
		row.reverse()
		row.push_back(cell)
		for r in row:
			print("Regenerating cell at (%d,%d)" % [r.x_position, r.y_position])
			generate_cell(r)
	else:
		cell.correct = weighted_choice(weights)
	# For debugging
#	cell.guess = cell.correct
	
	
func generate_board():
	# Generate the values for the board.
	for c in child_cells:
		if c is EntryCell:
			generate_cell(c)
			
	# Now collect the sums so they can be displayed.
	for c in child_cells:
		if c is SumCell:
			c.across_sum = sum_right(c)
			c.down_sum = sum_down(c)
	
	# Finally, select the first valid cell.
	for c in child_cells:
		if c is EntryCell:
			select_cell(c)
			break


func clear_board():
	# Clear all guesses from the board as a player-initiated reset.
	for c in child_cells:
		if c is EntryCell:
			c.guess = 0
			

func select_cell(cell):
	# Select this cell and deselect the currently selected one.
	if selected_cell:
		selected_cell.selected = false
	cell.selected = true
	selected_cell = cell


func cell_input(value: int):
	# Handle input for a cell. This includes checking the board, etc.
	if value < 0 or value > 9 or value == selected_cell.guess:
		return
	
	selected_cell.guess = value
	if value == selected_cell.correct:
		emit_signal("cell_correct")
	else:
		emit_signal("cell_incorrect")
		
	if is_board_complete():
		emit_signal("game_complete")
	else:
		emit_signal("game_incomplete")
		
	# TODO: Refactor this into something more manageable.
	if value != 0:
		emit_signal("play_sound", "Cell Input SFX")
	else:
		emit_signal("play_sound", "Cell Clear SFX")


func is_board_complete() -> bool:
	# Check to see whether all entry cells have been filled with the correct number.
	# This is written to back out at the earliest possible moment.
	var complete := true
	for c in child_cells:
		if c is EntryCell and c.correct > 0 and c.guess != c.correct:
			complete = false
			break
	return complete


func get_hint() -> bool:
	# Get a hint by filling in the currently selected cell.
	# If the selected cell is already correct, this returns false.
	if selected_cell.guess == selected_cell.correct:
		return false
	else:
		selected_cell.guess = selected_cell.correct
		if is_board_complete():
			emit_signal("game_complete")
		else:
			emit_signal("game_incomplete")
		return true


# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize()
	create_board()


func create_board():
	for c in child_cells:
		c.queue_free()
	child_cells = []
	selected_cell = null

	columns = int(sqrt(len(board_layout)))
	for i in len(board_layout):
		var c := board_layout[i]
		var cell : BoardCell
		if c == Types.CellType.ENTRY:
			cell = entry_cell_node.instantiate()
			cell.cell_clicked.connect(_on_cell_clicked)
		elif c == Types.CellType.SUM:
			cell = sum_cell_node.instantiate()
		cell.x_position = i % columns
		cell.y_position = i / columns
		add_child(cell)
		child_cells.push_back(cell)
	generate_board()
	set_custom_minimum_size(Vector2(columns * Types.CELL_SIZE, columns * Types.CELL_SIZE))


func change_board_colors(dark_color: Color, light_color: Color) -> void:
	for c in child_cells:
		if c is SumCell:
			c.background_color = dark_color
			c.foreground_color = light_color
			c.border_color = light_color
		elif c is EntryCell:
			c.background_color = light_color
			c.foreground_color = dark_color
			c.border_color = dark_color

func _input(event):
	for i in range(1,10):
		if Input.is_action_just_pressed("Cell Input %d" % i):
			cell_input(i)
	if Input.is_action_just_pressed("Cell Clear"):
		cell_input(0)
		
	# Arrow navigation handling, for keyboard users.
	# TODO: Also make this scroll
	if Input.is_action_just_pressed("ui_left"):
		var next := cell_left(selected_cell)
		while next:
			if next is EntryCell:
				select_cell(next)
				break
			else:
				next = cell_left(next)
	if Input.is_action_just_pressed("ui_right"):
		var next := cell_right(selected_cell)
		while next:
			if next is EntryCell:
				select_cell(next)
				break
			else:
				next = cell_right(next)
	if Input.is_action_just_pressed("ui_up"):
		var next := cell_above(selected_cell)
		while next:
			if next is EntryCell:
				select_cell(next)
				break
			else:
				next = cell_above(next)
	if Input.is_action_just_pressed("ui_down"):
		var next := cell_below(selected_cell)
		while next:
			if next is EntryCell:
				select_cell(next)
				break
			else:
				next = cell_below(next)


func random_entry():
	# Get a random number for the cell at a certain position.
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_cell_clicked(cell):
	if is_processing_input():
		select_cell(cell)
