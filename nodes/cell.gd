extends Control
class_name BoardCell

var x_position := -1
var y_position := -1

func _get_minimum_size():
	return Vector2(Types.CELL_SIZE, Types.CELL_SIZE)
