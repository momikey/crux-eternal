extends BoardCell
class_name EntryCell

signal cell_clicked(cell: EntryCell)

@export var background_color : Color = Color.WHITE
@export var foreground_color : Color = Color.BLACK
@export var border_color : Color = Color.BLACK


@export var guess := 0:
	get:
		return guess
	set(value):
		guess = value
		if $Label:
			$Label.text = str(value) if value > 0 else ""

@export var correct := 0
@export var show_correct := false
@export var selected := false:
	get:
		return selected
	set(value):
		selected = value
		if $Selection:
			$Selection.visible = value

# Called when the node enters the scene tree for the first time.
func _ready():
	guess = guess
	selected = selected
	$Label.add_theme_color_override("font_color", foreground_color)

func _draw():
	draw_rect(Rect2(Vector2(0,0), self.get_minimum_size()), background_color)
	draw_rect(Rect2(Vector2(0,0), self.get_minimum_size()), border_color, false, 2.0)

func _gui_input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.is_released():
			cell_clicked.emit(self)
