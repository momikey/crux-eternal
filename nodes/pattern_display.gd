extends MarginContainer
class_name PatternDisplay

signal clicked(PatternDisplay)

const unselected_style := preload("res://resources/puzzle_unselected.tres")
const selected_style := preload("res://resources/puzzle_selected.tres")

@export var index := 0
@export var image : Texture2D

@onready var index_label := $List/Index
@onready var size_label := $List/Size
@onready var pattern_texture := $List/Pattern
@onready var backdrop := $Backdrop

var pressed := false

# Called when the node enters the scene tree for the first time.
func _ready():
	index_label.text = str(index)
	var image_size := image.get_size()
	size_label.text = "%dx%d" % [image_size.x, image_size.y]
	pattern_texture.texture = image
	
	# Calculate an integer scale factor for the pattern image, for better display.
	# Then make the TextureRect scale the pattern by that factor.
	var scale_factor := 100 / int(image_size.x)
	pattern_texture.custom_minimum_size = image_size * scale_factor


func _gui_input(event):
	# TODO: Show hover background, respond tp clicks
	if event is InputEventScreenTouch and event.index == 0:
		if pressed and not event.pressed:
			emit_signal("clicked", self)
		pressed = event.pressed


func _on_mouse_entered():
	backdrop.add_theme_stylebox_override("panel", selected_style)
	pressed = false
	accept_event()


func _on_mouse_exited():
	backdrop.add_theme_stylebox_override("panel", unselected_style)
	pressed = false
	accept_event()
