extends Label
class_name PuzzleTimer

@export var elapsed_seconds := 0:
	get:
		return elapsed_seconds
	set(value):
		elapsed_seconds = value
		format_time()
		
@onready var timer := Timer.new()

func stop_timer():
	timer.paused = true
	
	
func reset_timer():
	elapsed_seconds = 0
	timer.start(1)
	timer.paused = false


func add_time(secs: int):
	elapsed_seconds += secs


# Format the total elapsed seconds into MM:SS.
func format_time():	
	text = TimeFormat.format(elapsed_seconds)


# Called when the node enters the scene tree for the first time.
func _ready():
	format_time()
	add_child(timer)
	timer.timeout.connect(_on_timeout)
	timer.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_timeout():
	elapsed_seconds += 1
