extends BoardCell
class_name SumCell

@export var background_color : Color = Color.BLACK
@export var foreground_color : Color = Color.WHITE
@export var border_color : Color = Color.WHITE


@export var across_sum := 0:
	get:
		return across_sum
	set(value):
		across_sum = value
		if $Across:
			$Across.text = str(value) if value > 0 else ""
			
@export var down_sum := 0:
	get:
		return down_sum
	set(value):
		down_sum = value
		if $Down:
			$Down.text = str(value) if value > 0 else ""

# Called when the node enters the scene tree for the first time.
func _ready():
	across_sum = across_sum
	down_sum = down_sum
	$Across.add_theme_color_override("font_color", foreground_color)
	$Down.add_theme_color_override("font_color", foreground_color)


func _draw():
	draw_rect(Rect2(Vector2(0,0), self.get_minimum_size()), background_color)
	draw_rect(Rect2(Vector2(0,0), self.get_minimum_size()), border_color, false, 2.0)
	if across_sum > 0 or down_sum > 0:
		draw_line(Vector2(0,0), Vector2(Types.CELL_SIZE, Types.CELL_SIZE), foreground_color, 1.0, true)
