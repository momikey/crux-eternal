extends Panel
class_name VictoryModal

@export var parent_pack : PuzzleSelectData
@export var player_time_seconds : int:
	get:
		return player_time_seconds
	set(value):
		player_time_seconds = value
		if player_time_label != null:
			player_time_label.text = TimeFormat.format(player_time_seconds)
			if player_time_seconds < best_time_seconds:
				new_record_label.show()
				best_time_container.hide()
			else:
				new_record_label.hide()
				best_time_container.show()
				
@export var best_time_seconds : int:
	get:
		return best_time_seconds
	set(value):
		best_time_seconds = value
		if best_time_label != null:
			best_time_label.text = TimeFormat.format(best_time_seconds)

@onready var player_time_label := $"Times Container/Your Time Container/Time"
@onready var best_time_label := $"Times Container/Best Time Container/Time"
@onready var best_time_container := $"Times Container/Best Time Container"
@onready var new_record_label := $"Times Container/New Record"

func _ready():
	player_time_seconds = player_time_seconds
	best_time_seconds = best_time_seconds


func _on_replay_button_pressed():
	hide()
	ScreenManager.change_screen(Constants.Screens.puzzle, parent_pack)


func _on_menu_button_pressed():
	hide()
	ScreenManager.change_screen(Constants.Screens.main_menu)
