extends Resource
class_name GameSettings

# Top-level game settings. These are held in a resource for easier save/load operations.

@export_range(0, 10) var main_volume := 10

const NAMED_PROPERTIES = ["main_volume"]


# Update an object with new values. This should allow future-proofing for saved data.
func update_from(other: Resource):
	for prop in NAMED_PROPERTIES:
		var p = other.get(prop)
		if p != null:
			set(prop, p)
