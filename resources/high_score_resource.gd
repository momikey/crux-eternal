extends Resource
class_name HighScoreResource

# Resource to track high scores and save them.

const RESOURCE_FILE_NAME = "user://highscores.cfg"

# We'll store the data in a config file.
var config := ConfigFile.new()


func load_config():
	var err = config.load(RESOURCE_FILE_NAME)
	if err != OK:
		# Problem loading the data file.
		# TODO: Handle errors.
		return


func save_config():
	config.save(RESOURCE_FILE_NAME)


func get_score_for_puzzle(pack: String, puzzle_index: int):
	# Get the best saved time for a puzzle.
	# If no time has been saved yet, returns null.
	return config.get_value(pack, String.num_int64(puzzle_index), null)


func save_score(pack: String, puzzle_index: int, elapsed_seconds: int) -> bool:
	# Saves the time for a puzzle *if* it's better than what is currently saved.
	# Returns whether the given time is better (and thus saved).
	var old_time = config.get_value(pack, String.num_int64(puzzle_index), null)
	if old_time == null or elapsed_seconds < old_time:
		config.set_value(pack, String.num_int64(puzzle_index), elapsed_seconds)
		save_config()
		return true
	else:
		return false
