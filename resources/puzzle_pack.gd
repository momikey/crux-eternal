extends Resource
class_name PuzzlePack

# A "puzzle pack" is a collection of puzzle board patterns with some metadata.

# The list of patterns, each as a black and white image.
@export var patterns : Array[Texture2D]

# A friendly name for the pack.
@export var name : String

# A code name for the pack, so it can be stored in places that don't allow certain characters.
@export var code_name : String

# A backdrop image for the pack.
@export var backdrop : Texture2D

# Theme colors to use for the pack
@export var dark_color : Color = Color.BLACK
@export var light_color : Color = Color.WHITE
