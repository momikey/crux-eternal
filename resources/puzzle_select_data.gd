extends Resource
class_name PuzzleSelectData

# Simple data class for initializing a new puzzle.

@export var puzzle_layout : Texture2D
@export var index : int
@export var pack_name : String
@export var pack_code_name : String
@export var pack_backdrop : Texture2D
@export var dark_color : Color = Color.BLACK
@export var light_color : Color = Color.WHITE

func _init(layout = null, idx = -1, pack = null):
	puzzle_layout = layout
	index = idx
	if pack != null:
		pack_name = pack.name
		pack_code_name = pack.code_name
		pack_backdrop = pack.backdrop
		dark_color = pack.dark_color
		light_color = pack.light_color
