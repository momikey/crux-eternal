extends GameScreen
class_name BoardSelectScreen

@export var puzzle_pack : PuzzlePack

const pattern_display = preload("res://nodes/pattern_display.tscn")

@onready var title_label := $"Scroller/Panel/Container/Pack Title"
@onready var pattern_container := $Scroller/Panel/Container/Patterns

func _ready():
	if puzzle_pack.name:
		title_label.text = puzzle_pack.name
	else:
		title_label.text = "(no title)"
		
	if puzzle_pack.backdrop:
		var style_box = StyleBoxTexture.new()
		style_box.texture = puzzle_pack.backdrop
		style_box.modulate_color = Color(1,1,1,0.4)
		$Scroller/Panel.add_theme_stylebox_override("panel", style_box)
		

	for i in puzzle_pack.patterns.size():
		var p := pattern_display.instantiate()
		# 1-based for display
		p.index = i + 1
		
		p.image = puzzle_pack.patterns[i]
		p.clicked.connect(_on_pattern_clicked)
		pattern_container.add_child(p)

func _on_pattern_clicked(pattern):
	var res := PuzzleSelectData.new(pattern.image, pattern.index, puzzle_pack)
	ScreenManager.change_screen(Constants.Screens.puzzle, res)
