extends GameScreen
class_name CreditsScreen

const CREDITS_FILENAME := "res://CREDITS"
var credits_content : String
@onready var textbox := $Panel/ScrollContainer/Credits


func load_credits():
	var file = FileAccess.open(CREDITS_FILENAME, FileAccess.READ)
	credits_content = file.get_as_text()


# Called when the node enters the scene tree for the first time.
func _ready():
	load_credits()
	textbox.text = credits_content


func _on_back_button_pressed():
	ScreenManager.change_screen(Constants.Screens.main_menu)
