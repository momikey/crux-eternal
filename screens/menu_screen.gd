extends GameScreen

@onready var version_label := $"Panel/Version Text"
@onready var help_modal := $"Help Modal"

# Called when the node enters the scene tree for the first time.
func _ready():
	version_label.text = "Version %s - © 2023 Michael H. Potter" % Constants.version_info.game_version


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_play_button_pressed():
	ScreenManager.change_screen(Constants.Screens.board_select)


func _on_exit_button_pressed():
	if OS.get_name() == "Android":
		get_tree().root.propagate_notification(NOTIFICATION_WM_GO_BACK_REQUEST)
	else:
		get_tree().root.propagate_notification(NOTIFICATION_WM_CLOSE_REQUEST)
	get_tree().quit()


func _on_options_button_pressed():
	ScreenManager.change_screen(Constants.Screens.options)


func _on_credits_button_pressed():
	ScreenManager.change_screen(Constants.Screens.credits)


func _on_help_button_pressed():
	help_modal.show()
	

func _on_help_dismiss_pressed():
	help_modal.hide()
