extends GameScreen

@onready var main_volume_slider := $"Panel/ScrollContainer/Options Container/Main Volume Container/Main Volumn Slider"

# Called when the node enters the scene tree for the first time.
func _ready():
	main_volume_slider.value = GlobalSettings.settings.main_volume


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_back_button_pressed():
	ScreenManager.change_screen(Constants.Screens.main_menu)


func _on_save_button_pressed():
	GlobalSettings.save_settings()
	ScreenManager.change_screen(Constants.Screens.main_menu)


func _on_main_volumn_slider_value_changed(value):
	GlobalSettings.change_main_volume(value)
