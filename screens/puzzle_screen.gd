extends GameScreen
class_name PuzzleScreen

var pack_data : PuzzleSelectData
var puzzle_best_time

@onready var board_scroll_container := $HBoxContainer/ScrollContainer
@onready var puzzle_board : PuzzleBoard = $HBoxContainer/ScrollContainer/PuzzleBoard
@onready var backdrop_panel := $Backdrop
@onready var control_panel := $HBoxContainer/Panel
@onready var puzzle_timer := $HBoxContainer/Panel/CenterContainer/PuzzleTimer
@onready var hint_sound := $"Cell Hint SFX"
@onready var best_time_label := $HBoxContainer/Panel/BestTimeContainer/BestTime
@onready var pause_modal := $"Pause Modal"
@onready var victory_modal := $"Victory Modal"

# Called when the node enters the scene tree for the first time.
func _ready():
	puzzle_board.play_sound.connect(_on_play_sound)


func _screen_setup(res: Resource = null):
	if res is PuzzleSelectData:
		pack_data = res
		var pixel_image : Image = res.puzzle_layout.get_image()
		var image_size = pixel_image.get_size()
		var cell_key : Array[int]
		for y in range(image_size.y):
			for x in range(image_size.x):
				cell_key.push_back(Constants.CellType.SUM if pixel_image.get_pixel(x,y) == Color.BLACK else Constants.CellType.ENTRY)
		puzzle_board.board_layout = cell_key
		puzzle_board.create_board()
		puzzle_board.change_board_colors(res.dark_color, res.light_color)
		
		if res.pack_backdrop:
			var style_box = StyleBoxTexture.new()
			style_box.texture = res.pack_backdrop
			style_box.modulate_color = Color(1,1,1,0.5)
			backdrop_panel.add_theme_stylebox_override("panel", style_box)
#			style_box.texture = AtlasTexture.new()
#			style_box.texture.atlas = res.pack_backdrop
#			var clip_rect := Rect2(res.pack_backdrop.get_width()*0.35, 0, res.pack_backdrop.get_width()*0.3, res.pack_backdrop.get_height())
#			style_box.texture.region = clip_rect
#			style_box.modulate_color = Color(1,1,1,0.4)
#			control_panel.add_theme_stylebox_override("panel", style_box)

		if res != null:
			puzzle_best_time = BestTimes.resource.get_score_for_puzzle(res.pack_code_name, res.index)
			if puzzle_best_time != null:
				show_best_time()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func show_best_time():
	if puzzle_best_time:
		best_time_label.text = "Best Time: %s" % TimeFormat.format(puzzle_best_time)
		best_time_label.show()


func _on_digit_button_pressed(digit: int):
	var ev := InputEventAction.new()
	ev.action = "Cell Input %d" % digit
	ev.pressed = true
	Input.parse_input_event(ev)
	await get_tree().create_timer(1/60.).timeout
	ev.pressed = false
	Input.parse_input_event(ev)


func _on_clear_button_pressed():
	var ev := InputEventAction.new()
	ev.action = "Cell Clear"
	ev.pressed = true
	Input.parse_input_event(ev)
	await get_tree().create_timer(1/60.).timeout
	ev.pressed = false
	Input.parse_input_event(ev)


func _on_play_sound(sound_node: String):
	find_child(sound_node).play()


func _on_puzzle_board_game_complete():
	$"HBoxContainer/Panel/Debug Progress".text = "You win!"
	puzzle_timer.stop_timer()
	BestTimes.resource.save_score(pack_data.pack_code_name, pack_data.index, puzzle_timer.elapsed_seconds)
	victory_modal.best_time_seconds = puzzle_best_time
	victory_modal.player_time_seconds = puzzle_timer.elapsed_seconds
	victory_modal.parent_pack = pack_data
	victory_modal.show()


func _on_puzzle_board_game_incomplete():
	$"HBoxContainer/Panel/Debug Progress".text = "Not done yet"


func _on_hint_button_pressed():
	if puzzle_board.get_hint():
		puzzle_timer.add_time(Constants.HINT_COST)
		hint_sound.play()


func _on_pause_button_pressed():
	get_tree().paused = true
	pause_modal.show()


func _on_scroll_container_scroll_started():
	puzzle_board.set_process_input(false)


func _on_scroll_container_scroll_ended():
	puzzle_board.set_process_input(true)
