extends MarginContainer
class_name GameScreen

func _screen_setup(res: Resource = null) -> void:
	# Hook for screens to customize behavior after they have been switched to.
	pass
