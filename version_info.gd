extends Resource
class_name VersionInfoResource

# Just some basic version info for the game.

@export var game_version : String
